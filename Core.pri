#-------------------------------------------------
#
# Core.pri created by Shrademn
#
#-------------------------------------------------

INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD

SOURCES += \
	$$PWD/Duration.cpp \
	$$PWD/SApplication.cpp \
	$$PWD/Settings.cpp

HEADERS += \
	$$PWD/Duration.hpp \
	$$PWD/SApplication.hpp \
	$$PWD/Settings.hpp \
	$$PWD/Settings.tpp \
	$$PWD/core.tpp \
	$$PWD/operators.hpp \
	$$PWD/resources.hpp

RESOURCES += $$PWD/Core.qrc
