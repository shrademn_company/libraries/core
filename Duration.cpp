//------------------------------------------------------------------------------
//
// Duration.cpp created by Yyhrs 2020-01-15
//
//------------------------------------------------------------------------------

#include <QRegularExpression>

#include "Duration.hpp"

Duration::Duration(qint64 ms):
	m_duration{ms}
{
}

Duration::Duration(qint64 h, qint64 min, qint64 s, qint64 ms):
	m_duration{h * 3600000 + min * 60000 + s * 1000 + ms}
{
}

Duration &Duration::operator+=(Duration const &duration)
{
	m_duration += duration.m_duration;
	return *this;
}

qint64 Duration::h() const
{
	return m_duration / 3600000;
}

qint64 Duration::min() const
{
	return m_duration % 3600000 / 60000;
}

qint64 Duration::s() const
{
	return m_duration % 60000 / 1000;
}

qint64 Duration::ms() const
{
	return m_duration % 1000;
}

qreal Duration::toH() const
{
	return m_duration / 3600000.;
}

qreal Duration::toM() const
{
	return m_duration / 60000.;
}

qreal Duration::toS() const
{
	return m_duration / 1000.;
}

qint64 Duration::toMs() const
{
	return m_duration;
}

QString Duration::toString(Precision precision) const
{
	static QString const hours{"%1"};
	static QString const minutes{"%1:%2"};
	static QString const seconds{"%1:%2:%3"};
	static QString const milliseconds{"%1:%2:%3.%4"};

	if (precision == H)
		return hours.arg(h(), 2, 10, QChar{'0'});
	if (precision == Min)
		return minutes.arg(h(), 2, 10, QChar{'0'}).arg(min(), 2, 10, QChar{'0'});
	else if (precision == S)
		return seconds.arg(h(), 2, 10, QChar{'0'}).arg(min(), 2, 10, QChar{'0'}).arg(s(), 2, 10, QChar{'0'});
	else
		return milliseconds.arg(h(), 2, 10, QChar{'0'}).arg(min(), 2, 10, QChar{'0'}).arg(s(), 2, 10, QChar{'0'}).arg(ms(), 3, 10, QChar{'0'});
}

Duration Duration::fromH(qreal h)
{
	return Duration{qRound(h * 3600000)};
}

Duration Duration::fromMin(qreal min)
{
	return Duration{qRound(min * 60000)};
}

Duration Duration::fromS(qreal s)
{
	return Duration{qRound(s * 1000)};
}

Duration Duration::fromMs(qint64 ms)
{
	return Duration{ms};
}

Duration Duration::fromString(QString const &duration)
{
	auto const split{duration.split(QRegularExpression{"[:\\.]"})};

	return Duration{split.value(0).toInt(), split.value(1).toInt(), split.value(2).toInt(), split.value(3).toInt()};
}

QDebug operator<<(QDebug debug, Duration const &duration)
{
	debug.nospace() << "Duration(" + duration.toString() + ")";
	return debug.maybeSpace();
}
