//------------------------------------------------------------------------------
//
// Settings.cpp created by Yyhrs 15
//
//------------------------------------------------------------------------------

#include <QHeaderView>

#include "Settings.hpp"

Settings::Settings(QSettings::Scope scope, QString const &organization, QObject *parent):
	QSettings{QSettings::IniFormat, scope, organization, qAppName(), parent}
{
}

Settings::Settings(QString const &fileName, QObject *parent):
	QSettings{fileName, QSettings::IniFormat, parent}
{
}

QVariantMap Settings::groupValues(QString const &group)
{
	QVariantMap result;

	beginGroup(group);
	for (auto &key: allKeys())
		result[key] = value(key);
	endGroup();
	return result;
}

void Settings::setGroupValues(QString const &group, QVariantMap const &values)
{
	beginGroup(group);
	for (auto const &key: values.keys())
		setValue(key, values[key]);
	endGroup();
}

void Settings::restoreState(QMainWindow *mainWindow) const
{
	mainWindow->restoreGeometry(value(Geometry).toByteArray());
	mainWindow->restoreState(value(State).toByteArray());
}

void Settings::saveState(QMainWindow const *mainWindow)
{
	setValue(Geometry, mainWindow->saveGeometry());
	setValue(State, mainWindow->saveState());
}

bool Settings::restoreState(QString const &key, QTableView *tableView) const
{
	return tableView->horizontalHeader()->restoreState(value(key).toByteArray());
}

void Settings::saveState(QString const &key, QTableView const *tableView)
{
	setValue(key, tableView->horizontalHeader()->saveState());
}

void Settings::restoreValues(QString const &key, QComboBox *combobox, QStringList const defaultValues) const
{
	combobox->addItems(value(key, defaultValues).toStringList());
}

void Settings::saveValues(QString const &key, QComboBox const *combobox)
{
	QStringList items;

	for (int index{0}; index < combobox->count(); ++index)
		items << combobox->itemText(index);
	setValue(key, items);
}

void Settings::restoreValues(QString const &key, QListWidget *listWidget, QStringList const defaultValues) const
{
	listWidget->addItems(value(key, defaultValues).toStringList());
}

void Settings::saveValues(QString const &key, QListWidget const *listWidget)
{
	QStringList items;

	for (int index{0}; index < listWidget->count(); ++index)
		items << listWidget->item(index)->text();
	setValue(key, items);
}
