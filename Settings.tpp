//------------------------------------------------------------------------------
//
// Settings.tpp created by Yyhrs 15
//
//------------------------------------------------------------------------------

#include "Settings.hpp"

template<typename Key>
Settings::Return<Key, QVariant> Settings::value(Key key, QVariant const &defaultValue) const
{
	return value(QMetaEnum::fromType<Key>().valueToKey(key), defaultValue);
}

template<typename Key>
Settings::Return<Key, void> Settings::setValue(Key key, QVariant const &value)
{
	setValue(QMetaEnum::fromType<Key>().valueToKey(key), value);
}

template<typename Key>
Settings::Return<Key, void> Settings::remove(Key key)
{
	remove(QMetaEnum::fromType<Key>().valueToKey(key));
}

template<typename Key>
Settings::Return<Key, void> Settings::beginGroup(Key group)
{
	beginGroup(QMetaEnum::fromType<Key>().valueToKey(group));
}

template<typename Key>
Settings::Return<Key, QVariantMap> Settings::groupValues(Key group)
{
	return groupValues(QMetaEnum::fromType<Key>().valueToKey(group));
}

template<typename Key>
Settings::Return<Key, void> Settings::setGroupValues(Key group, QVariantMap const &values)
{
	groupValues(QMetaEnum::fromType<Key>().valueToKey(group), values);
}

template<typename Key>
Settings::Return<Key, bool> Settings::restoreState(Key key, QTableView *tableView) const
{
	return restoreState(QString(QMetaEnum::fromType<Key>().valueToKey(key)), tableView);
}

template<typename Key>
Settings::Return<Key, void> Settings::saveState(Key key, QTableView const *tableView)
{
	saveState(QString(QMetaEnum::fromType<Key>().valueToKey(key)), tableView);
}

template<typename Key>
Settings::Return<Key, void> Settings::restoreValues(Key key, QComboBox *combobox, QStringList const defaultValues) const
{
	restoreValues(QString(QMetaEnum::fromType<Key>().valueToKey(key)), combobox, defaultValues);
}

template<typename Key>
Settings::Return<Key, void> Settings::saveValues(Key key, QComboBox const *combobox)
{
	saveValues(QString(QMetaEnum::fromType<Key>().valueToKey(key)), combobox);
}

template<typename Key>
Settings::Return<Key, void> Settings::restoreValues(Key key, QListWidget *listWidget, QStringList const defaultValues) const
{
	restoreValues(QString(QMetaEnum::fromType<Key>().valueToKey(key)), listWidget, defaultValues);
}

template<typename Key>
Settings::Return<Key, void> Settings::saveValues(Key key, QListWidget const *listWidget)
{
	saveValues(QString(QMetaEnum::fromType<Key>().valueToKey(key)), listWidget);
}
