//------------------------------------------------------------------------------
//
// operators.hpp created by Yyhrs 2019-10-24
//
//------------------------------------------------------------------------------

#ifndef OPERATORS_HPP
#define OPERATORS_HPP

#include <QDir>

inline QString operator/(QString const &parent, QString const &child)
{
	return parent + '/' + child;
}

inline QString operator/(QFileInfo const &parent, QFileInfo const &child)
{
	return parent.path() / child.filePath();
}

inline QString operator/(QDir const &root, QString const &child)
{
	return root.absolutePath() / child;
}

inline QString operator/(QDir const &root, QFileInfo const &child)
{
	return root.absolutePath() / child.filePath();
}

inline QString operator/(QDir const &root, QDir const &child)
{
	return root.absolutePath() / child.path();
}

#endif // OPERATORS_HPP
