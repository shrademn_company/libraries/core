//------------------------------------------------------------------------------
//
// SApplication.hpp created by Yyhrs 2022/10/06
//
//------------------------------------------------------------------------------

#ifndef SAPPLICATION_H
#define SAPPLICATION_H

#include <QApplication>
#include <QMainWindow>
#include <QSplashScreen>

#include "resources.hpp"
#include "Settings.hpp"

#define sApp (static_cast<SApplication *>(QCoreApplication::instance()))

class SApplication: public QApplication
{
	Q_OBJECT

public:
	enum SettingsKey
	{
		Theme
	};
	Q_ENUM(SettingsKey)

	explicit SApplication(int &argc, char *argv[]);

	void startSplashScreen(QString const &picture = Logos::c_shrademnCompanyRed.arg(Theme::c_light));
	void stopSplashScreen(QMainWindow *window = nullptr);

	Settings &settings();

	static QString  getStyleSheet();
	static QPalette getPalette();

	static void setTheme(QString const &theme);
	static void cycleTheme();

	static inline QString const s_colorBlue{QStringLiteral("#FF3DAEE9")};
	static inline QString const s_colorGreen{QStringLiteral("#FF27AE60")};
	static inline QString const s_colorRed{QStringLiteral("#FFDA4453")};
	static inline QString const s_colorYellow{QStringLiteral("#FFEDC11E")};
	static inline QString const s_colorDarkBright{QStringLiteral("#FF555555")};
	static inline QString const s_colorDark{QStringLiteral("#FF4D4D4D")};
	static inline QString const s_colorDarkDim{QStringLiteral("#FF373737")};
	static inline QString const s_colorLightBright{QStringLiteral("#FFF7F8F9")};
	static inline QString const s_colorLight{QStringLiteral("#FFEFF0F1")};
	static inline QString const s_colorLightDim{QStringLiteral("#FFE7E8E9")};

private:
	QSplashScreen m_splash;
	Settings      m_settings;
};

#endif // SAPPLICATION_H
