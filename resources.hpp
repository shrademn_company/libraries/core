//---------------------------------------
//
// resources.hpp created by Yyhrs 2019-08-26
//
//---------------------------------------

#ifndef RESOURCES_HPP
#define RESOURCES_HPP

#include <QIcon>
#include <QString>

namespace Theme
{
	inline const QString c_dark{QStringLiteral("dark")};
	inline const QString c_light{QStringLiteral("light")};
}

namespace Icons
{
	inline const QString c_add{QStringLiteral(":/%1/icons/add.svg")};
	inline const QString c_backward{QStringLiteral(":/%1/icons/backward.svg")};
	inline const QString c_cancelRed{QStringLiteral(":/%1/icons/cancel-red.svg")};
	inline const QString c_cancel{QStringLiteral(":/%1/icons/cancel.svg")};
	inline const QString c_columnsBlue{QStringLiteral(":/%1/icons/columns-blue.svg")};
	inline const QString c_columns{QStringLiteral(":/%1/icons/columns.svg")};
	inline const QString c_creditYellow{QStringLiteral(":/%1/icons/credit-yellow.svg")};
	inline const QString c_credit{QStringLiteral(":/%1/icons/credit.svg")};
	inline const QString c_deleteRed{QStringLiteral(":/%1/icons/delete-red.svg")};
	inline const QString c_delete{QStringLiteral(":/%1/icons/delete.svg")};
	inline const QString c_detach{QStringLiteral(":/%1/icons/detach.svg")};
	inline const QString c_document{QStringLiteral(":/%1/icons/document.svg")};
	inline const QString c_downSmall{QStringLiteral(":/%1/icons/down-small.svg")};
	inline const QString c_download{QStringLiteral(":/%1/icons/download.svg")};
	inline const QString c_down{QStringLiteral(":/%1/icons/down.svg")};
	inline const QString c_file{QStringLiteral(":/%1/icons/file.svg")};
	inline const QString c_filter{QStringLiteral(":/%1/icons/filter.svg")};
	inline const QString c_financeYellow{QStringLiteral(":/%1/icons/finance-yellow.svg")};
	inline const QString c_finance{QStringLiteral(":/%1/icons/finance.svg")};
	inline const QString c_flagBlack{QStringLiteral(":/%1/icons/flag-black.svg")};
	inline const QString c_flagBlue{QStringLiteral(":/%1/icons/flag-blue.svg")};
	inline const QString c_flagGreen{QStringLiteral(":/%1/icons/flag-green.svg")};
	inline const QString c_flagRed{QStringLiteral(":/%1/icons/flag-red.svg")};
	inline const QString c_flagYellow{QStringLiteral(":/%1/icons/flag-yellow.svg")};
	inline const QString c_flag{QStringLiteral(":/%1/icons/flag.svg")};
	inline const QString c_folderBlack{QStringLiteral(":/%1/icons/folder-black.svg")};
	inline const QString c_folderBlue{QStringLiteral(":/%1/icons/folder-blue.svg")};
	inline const QString c_folderGreen{QStringLiteral(":/%1/icons/folder-green.svg")};
	inline const QString c_folderRed{QStringLiteral(":/%1/icons/folder-red.svg")};
	inline const QString c_folderYellow{QStringLiteral(":/%1/icons/folder-yellow.svg")};
	inline const QString c_folder{QStringLiteral(":/%1/icons/folder.svg")};
	inline const QString c_forward{QStringLiteral(":/%1/icons/forward.svg")};
	inline const QString c_gearsAnimate{QStringLiteral(":/%1/icons/gears-animate.svg")};
	inline const QString c_gears{QStringLiteral(":/%1/icons/gears.svg")};
	inline const QString c_gear{QStringLiteral(":/%1/icons/gear.svg")};
	inline const QString c_hideRed{QStringLiteral(":/%1/icons/hide-red.svg")};
	inline const QString c_horizontal{QStringLiteral(":/%1/icons/horizontal.svg")};
	inline const QString c_leftSmall{QStringLiteral(":/%1/icons/left-small.svg")};
	inline const QString c_left{QStringLiteral(":/%1/icons/left.svg")};
	inline const QString c_librarYellow{QStringLiteral(":/%1/icons/librar-yellow.svg")};
	inline const QString c_libraryBlack{QStringLiteral(":/%1/icons/library-black.svg")};
	inline const QString c_libraryBlue{QStringLiteral(":/%1/icons/library-blue.svg")};
	inline const QString c_libraryGreen{QStringLiteral(":/%1/icons/library-green.svg")};
	inline const QString c_libraryRed{QStringLiteral(":/%1/icons/library-red.svg")};
	inline const QString c_library{QStringLiteral(":/%1/icons/library.svg")};
	inline const QString c_load{QStringLiteral(":/%1/icons/load.svg")};
	inline const QString c_minus{QStringLiteral(":/%1/icons/minus.svg")};
	inline const QString c_next{QStringLiteral(":/%1/icons/next.svg")};
	inline const QString c_pause{QStringLiteral(":/%1/icons/pause.svg")};
	inline const QString c_penBlack{QStringLiteral(":/%1/icons/pen-black.svg")};
	inline const QString c_penBlue{QStringLiteral(":/%1/icons/pen-blue.svg")};
	inline const QString c_penGreen{QStringLiteral(":/%1/icons/pen-green.svg")};
	inline const QString c_penRed{QStringLiteral(":/%1/icons/pen-red.svg")};
	inline const QString c_penYellow{QStringLiteral(":/%1/icons/pen-yellow.svg")};
	inline const QString c_pen{QStringLiteral(":/%1/icons/pen.svg")};
	inline const QString c_playGreen{QStringLiteral(":/%1/icons/play-green.svg")};
	inline const QString c_play{QStringLiteral(":/%1/icons/play.svg")};
	inline const QString c_plus{QStringLiteral(":/%1/icons/plus.svg")};
	inline const QString c_previous{QStringLiteral(":/%1/icons/previous.svg")};
	inline const QString c_refresh{QStringLiteral(":/%1/icons/refresh.svg")};
	inline const QString c_remove{QStringLiteral(":/%1/icons/remove.svg")};
	inline const QString c_renameGreen{QStringLiteral(":/%1/icons/rename-green.svg")};
	inline const QString c_rename{QStringLiteral(":/%1/icons/rename.svg")};
	inline const QString c_rightSmall{QStringLiteral(":/%1/icons/right-small.svg")};
	inline const QString c_right{QStringLiteral(":/%1/icons/right.svg")};
	inline const QString c_rowsBlue{QStringLiteral(":/%1/icons/rows-blue.svg")};
	inline const QString c_rows{QStringLiteral(":/%1/icons/rows.svg")};
	inline const QString c_show{QStringLiteral(":/%1/icons/show.svg")};
	inline const QString c_split{QStringLiteral(":/%1/icons/split.svg")};
	inline const QString c_spreadsheetGreen{QStringLiteral(":/%1/icons/spreadsheet-green.svg")};
	inline const QString c_spreadsheet{QStringLiteral(":/%1/icons/spreadsheet.svg")};
	inline const QString c_stopRed{QStringLiteral(":/%1/icons/stop-red.svg")};
	inline const QString c_stop{QStringLiteral(":/%1/icons/stop.svg")};
	inline const QString c_textBold{QStringLiteral(":/%1/icons/textBold.svg")};
	inline const QString c_textCenter{QStringLiteral(":/%1/icons/textCenter.svg")};
	inline const QString c_textItalic{QStringLiteral(":/%1/icons/textItalic.svg")};
	inline const QString c_textJustify{QStringLiteral(":/%1/icons/textJustify.svg")};
	inline const QString c_textLeft{QStringLiteral(":/%1/icons/textLeft.svg")};
	inline const QString c_textRight{QStringLiteral(":/%1/icons/textRight.svg")};
	inline const QString c_textUnderline{QStringLiteral(":/%1/icons/text-underline.svg")};
	inline const QString c_time{QStringLiteral(":/%1/icons/time.svg")};
	inline const QString c_toggle{QStringLiteral(":/%1/icons/toggle.svg")};
	inline const QString c_unfilterRed{QStringLiteral(":/%1/icons/unfilter-red.svg")};
	inline const QString c_unload{QStringLiteral(":/%1/icons/unload.svg")};
	inline const QString c_upSmall{QStringLiteral(":/%1/icons/up-small.svg")};
	inline const QString c_upload{QStringLiteral(":/%1/icons/upload.svg")};
	inline const QString c_up{QStringLiteral(":/%1/icons/up.svg")};
	inline const QString c_vertical{QStringLiteral(":/%1/icons/vertical.svg")};
	inline const QString c_windows{QStringLiteral(":/%1/icons/windows.svg")};
	inline const QString c_x{QStringLiteral(":/%1/icons/x.svg")};
}
namespace Logos
{
	inline const QString c_sc{QStringLiteral(":/%1/logos/sc.svg")};
	inline const QString c_shrademnCompany{QStringLiteral(":/%1/logos/shrademn-company.svg")};
	inline const QString c_shrademnCompanyAnimateRed{QStringLiteral(":/%1/logos/shrademn-company-animate-red.svg")};
	inline const QString c_shrademnCompanyRed{QStringLiteral(":/%1/logos/shrademn-company-red.svg")};
}
namespace Stylesheets
{
	inline const QString c_shrademnCompany{QStringLiteral(":/stylesheets/shrademn-company.qss")};
}

#endif // RESOURCES_HPP
