//------------------------------------------------------------------------------
//
// core.tpp created by Yyhrs 2019-10-17
//
//------------------------------------------------------------------------------

#ifndef CORE_TPP
#define CORE_TPP

#include <QDesktopServices>
#include <QDir>
#include <QFileInfo>
#include <QMetaEnum>
#include <QProcess>
#include <QRegularExpression>
#include <QUrl>

template<typename Value>
constexpr const Value &adapt(Value &value)
{
	return value;
}

template<typename Value>
constexpr const Value adapt(Value &&value) noexcept
{
	return std::forward<Value>(value);
}

namespace Enum
{

template<typename Value, typename T>
using Return = typename std::enable_if<QtPrivate::IsQEnumHelper<Value>::Value, T>::type;

template<typename Value>
inline Return<Value, QString> string(Value value)
{
	return QMetaEnum::fromType<Value>().valueToKey(value);
};

template<typename Value>
inline Return<Value, QString> string(int value)
{
	return string(static_cast<Value>(value));
};

template<typename Value>
inline Return<Value, int> number(const QString &key)
{
	return QMetaEnum::fromType<Value>().keyToValue(key.toLatin1());
};

template<typename Value>
inline Return<Value, Value> value(const QString &key)
{
	return static_cast<Value>(number<Value>(key));
};

}

namespace File
{

inline void showInGraphicalShell(const QFileInfo &file)
{
#ifdef Q_OS_LINUX
	QDesktopServices::openUrl(QUrl::fromLocalFile(file.absolutePath()));
#endif
#ifdef Q_OS_MACOS
	QDesktopServices::openUrl(QUrl::fromLocalFile(file.absolutePath()));
#endif
#ifdef Q_OS_WIN
	QProcess::startDetached("explorer", {"/select,", QDir::toNativeSeparators(file.absoluteFilePath())});
#endif
}

inline bool overwriteCopy(const QString &source, const QString &destination)
{
	if (QFile::exists(destination))
		return QFile::remove(destination) && QFile::copy(source, destination);
	return QFile::copy(source, destination);
}

inline bool move(const QString &source, const QString &destination)
{
	return QDir{}.mkpath(QFileInfo {destination}.absolutePath()) && QFile::rename(source, destination);
}

inline bool isValid(const QString &name)
{
	static const QRegularExpression validator{R"(^(?!(?:CON|PRN|AUX|NUL|COM[1-9]|LPT[1-9])(?:\.[^.]*)?$)[^<>:"/\\|?*\x00-\x1F]*[^<>:"/\\|?*\x00-\x1F\ .]$)", QRegularExpression::CaseInsensitiveOption};

	return validator.match(name).hasMatch();
}

inline QString getValid(const QString &name)
{
	static const QRegularExpression prohibitedNames{R"(^(CON|PRN|AUX|NUL|COM[1-9]|LPT[1-9])$)", QRegularExpression::CaseInsensitiveOption};
	static const QRegularExpression prohibitedCharacters{R"([<>:"\/\\|?*\x00-\x1F]|^[ .]|[ .]$)", QRegularExpression::CaseInsensitiveOption};
	QString                         result{name};

	if (prohibitedNames.match(result).hasMatch())
		result.append('_');
	return result.replace(prohibitedCharacters, "_");
}

inline QFileInfoList getFiles(const QFileInfo &file, const QStringList &filters = {}, bool recursive = false)
{
	QFileInfoList files;

	if (file.isDir())
	{
		QDir dir{file.absoluteFilePath()};

		for (const auto &fileInfo: adapt(dir.entryInfoList(filters, QDir::Files)))
			files << fileInfo;
		if (recursive)
			for (const auto &fileInfo: adapt(dir.entryInfoList(QDir::Dirs | QDir::NoDotAndDotDot)))
				files << getFiles(fileInfo, filters, recursive);
	}
	else
		files << file;
	return files;
}

}

namespace Container
{

template<typename Key, typename Value>
class WrapperKeyValue
{
public:
	inline WrapperKeyValue(const QMap<Key, Value> &container): m_reference{container}
	{
	}

	inline WrapperKeyValue(const QMap<Key, Value> &&container): m_keep{container}, m_reference{m_keep}
	{
	}

	auto begin() const
	{
		return m_reference.keyValueBegin();
	}

	auto end() const
	{
		return m_reference.keyValueEnd();
	}

private:
	const QMap<Key, Value> &m_reference;
	const QMap<Key, Value> m_keep;
};

template<typename Key, typename Value>
class WrapperKey
{
public:
	inline WrapperKey(const QMap<Key, Value> &container): m_reference{container}
	{
	}

	inline WrapperKey(const QMap<Key, Value> &&container): m_keep{container}, m_reference{m_keep}
	{
	}

	auto begin() const
	{
		return m_reference.keyBegin();
	}

	auto end() const
	{
		return m_reference.keyEnd();
	}

private:
	const QMap<Key, Value> &m_reference;
	const QMap<Key, Value> m_keep;
};

}

template<typename Key, typename Value>
constexpr auto keyValues(const QMap<Key, Value> &container) noexcept
{
	return Container::WrapperKeyValue{container};
}

template<typename Key, typename Value>
constexpr auto keys(const QMap<Key, Value> &container) noexcept
{
	return Container::WrapperKey{container};
}

#endif // CORE_TPP
