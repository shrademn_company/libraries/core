//------------------------------------------------------------------------------
//
// Settings.hpp created by Yyhrs 15
//
//------------------------------------------------------------------------------

#ifndef SETTINGS_HPP
#define SETTINGS_HPP

#include <QComboBox>
#include <QCoreApplication>
#include <QListWidget>
#include <QMainWindow>
#include <QMetaEnum>
#include <QSettings>
#include <QTableView>

class Settings: public QSettings
{
	Q_OBJECT

public:
	enum Window
	{
		Geometry,
		State
	};
	Q_ENUM(Window)

	template<typename Key, typename T>
	using Return = typename std::enable_if<QtPrivate::IsQEnumHelper<Key>::Value, T>::type;

	Settings(QSettings::Scope scope = QSettings::UserScope, QString const &organization = QStringLiteral("SC"), QObject *parent = nullptr);
	Settings(QString const &fileName, QObject *parent = nullptr);
	virtual ~Settings() = default;

	using QSettings::value;
	template<typename Key>
	Return<Key, QVariant> value(Key key, QVariant const &defaultValue = QVariant{}) const;

	using QSettings::setValue;
	template<typename Key>
	Return<Key, void> setValue(Key key, QVariant const &value);

	using QSettings::remove;
	template<typename Key>
	Return<Key, void> remove(Key key);

	using QSettings::beginGroup;
	template<typename Key>
	Return<Key, void> beginGroup(Key group);

	QVariantMap groupValues(QString const &group);
	template<typename Key>
	Return<Key, QVariantMap> groupValues(Key group);
	void                     setGroupValues(QString const &group, QVariantMap const &values);
	template<typename Key>
	Return<Key, void> setGroupValues(Key group, QVariantMap const &values);

	void restoreState(QMainWindow *mainWindow) const;
	void saveState(QMainWindow const *mainWindow);

	bool restoreState(QString const &key, QTableView *tableView) const;
	template<typename Key>
	Return<Key, bool> restoreState(Key key, QTableView *tableView) const;
	void              saveState(QString const &key, QTableView const *tableView);
	template<typename Key>
	Return<Key, void> saveState(Key key, QTableView const *tableView);

	void restoreValues(QString const &key, QComboBox *combobox, QStringList const defaultValues) const;
	template<typename Key>
	Return<Key, void> restoreValues(Key key, QComboBox *combobox, QStringList const defaultValues) const;
	void              saveValues(QString const &key, QComboBox const *combobox);
	template<typename Key>
	Return<Key, void> saveValues(Key key, QComboBox const *combobox);
	void              restoreValues(QString const &key, QListWidget *listWidget, QStringList const defaultValues) const;
	template<typename Key>
	Return<Key, void> restoreValues(Key key, QListWidget *listWidget, QStringList const defaultValues) const;
	void              saveValues(QString const &key, QListWidget const *listWidget);
	template<typename Key>
	Return<Key, void> saveValues(Key key, QListWidget const *listWidget);
};

#include "Settings.tpp"

#endif // SETTINGS_HPP
