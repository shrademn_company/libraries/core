//------------------------------------------------------------------------------
//
// Duration.hpp created by Yyhrs 2020-01-15
//
//------------------------------------------------------------------------------

#ifndef DURATION_HPP
#define DURATION_HPP

#include <QDebug>
#include <QMetaType>

class Duration
{
public:
	enum Precision
	{
		H,
		Min,
		S,
		Ms
	};

	Duration() = default;
	Duration(qint64 ms);
	Duration(qint64 h, qint64 min, qint64 s, qint64 ms);
	Duration(Duration const &duration) = default;
	~Duration() = default;

	Duration &operator=(Duration const &duration) = default;
	Duration &operator+=(Duration const &duration);

	qint64  h() const;
	qint64  min() const;
	qint64  s() const;
	qint64  ms() const;
	qreal   toH() const;
	qreal   toM() const;
	qreal   toS() const;
	qint64  toMs() const;
	QString toString(Precision precision = S) const;

	static Duration fromH(qreal h);
	static Duration fromMin(qreal min);
	static Duration fromS(qreal s);
	static Duration fromMs(qint64 ms);
	static Duration fromString(QString const &duration);

private:
	qint64 m_duration{0};
};

Q_DECLARE_METATYPE(Duration);

QDebug operator<<(QDebug debug, Duration const &duration);

#endif // DURATION_HPP
